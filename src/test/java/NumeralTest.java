import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class NumeralTest {
    Numeral num;

    @Test
    void should_convert_zero(){
        num = new Numeral("0");
        assertEquals("zéro", num.toLetters());
    }

    @Test
    void should_convert_one(){
        num = new Numeral("1");
        assertEquals("un", num.toLetters());
    }

    @Test
    void should_convert_two(){
        num = new Numeral("2");
        assertEquals("deux", num.toLetters());
    }

    @Test
    void should_convert_three(){
        num = new Numeral("3");
        assertEquals("trois", num.toLetters());
    }

    @Test
    void should_convert_four(){
        num = new Numeral("4");
        assertEquals("quatre", num.toLetters());
    }

    @Test
    void should_convert_five(){
        num = new Numeral("5");
        assertEquals("cinq", num.toLetters());
    }

    @Test
    void should_convert_six(){
        num = new Numeral("6");
        assertEquals("six", num.toLetters());
    }

    @Test
    void should_convert_seven(){
        num = new Numeral("7");
        assertEquals("sept", num.toLetters());
    }

    @Test
    void should_convert_eight(){
        num = new Numeral("8");
        assertEquals("huit", num.toLetters());
    }

    @Test
    void should_convert_nine(){
        num = new Numeral("9");
        assertEquals("neuf", num.toLetters());
    }

    @Test
    void should_convert_ten(){
        num = new Numeral("10");
        assertEquals("dix", num.toLetters());
    }

    @Test
    void should_convert_elf(){
        num = new Numeral("11");
        assertEquals("onze", num.toLetters());
    }

    @Test
    void should_convert_twelve(){
        num = new Numeral("12");
        assertEquals("douze", num.toLetters());
    }

    @Test
    void should_convert_thirteen(){
        num = new Numeral("13");
        assertEquals("treize", num.toLetters());
    }

    @Test
    void should_convert_fourteen(){
        num = new Numeral("14");
        assertEquals("quatorze", num.toLetters());
    }

    @Test
    void should_convert_fifteen(){
        num = new Numeral("15");
        assertEquals("quinze", num.toLetters());
    }

    @Test
    void should_convert_sixteen(){
        num = new Numeral("16");
        assertEquals("seize", num.toLetters());
    }

    @Test
    void should_convert_seventeen_to_nineteen(){
        num = new Numeral("17");
        assertEquals("dix-sept", num.toLetters());
        num = new Numeral("18");
        assertEquals("dix-huit", num.toLetters());
        num = new Numeral("19");
        assertEquals("dix-neuf", num.toLetters());
    }

    @Test
    void should_convert_twenty_to_twentynine(){
        num = new Numeral("20");
        assertEquals("vingt", num.toLetters());
        num = new Numeral("23");
        assertEquals("vingt-trois", num.toLetters());
        num = new Numeral("26");
        assertEquals("vingt-six", num.toLetters());
    }

    @Test
    void should_convert_thirty_to_ninety(){
        num = new Numeral("30");
        assertEquals("trente", num.toLetters());
        num = new Numeral("45");
        assertEquals("quarante-cinq", num.toLetters());
        num = new Numeral("99");
        assertEquals("quatre-vingt-dix-neuf", num.toLetters());
    }

    @Test
    void should_convert_seventy(){
        num = new Numeral("70");
        assertEquals("soixante-dix", num.toLetters());
    }

    @Test
    void should_convert_seventyone(){
        num = new Numeral("71");
        assertEquals("soixante-et-onze", num.toLetters());
    }

    @Test
    void should_convert_seventyseven(){
        num = new Numeral("77");
        assertEquals("soixante-dix-sept", num.toLetters());
    }

    @Test
    void should_convert_ninety(){
        num = new Numeral("90");
        assertEquals("quatre-vingt-dix", num.toLetters());
    }

    @Test
    void should_convert_ninetyone(){
        num = new Numeral("91");
        assertEquals("quatre-vingt-onze", num.toLetters());
    }

    @Test
    void should_convert_ninetyseven(){
        num = new Numeral("97");
        assertEquals("quatre-vingt-dix-sept", num.toLetters());
    }

    @Test
    void should_convert_eighty(){
        num = new Numeral("80");
        assertEquals("quatre-vingts", num.toLetters());
    }

    @Test
    void should_convert_onehundred(){
        num = new Numeral("100");
        assertEquals("cent", num.toLetters());
        num = new Numeral("101");
        assertEquals("cent un", num.toLetters());
        num = new Numeral("111");
        assertEquals("cent onze", num.toLetters());
        num = new Numeral("189");
        assertEquals("cent quatre-vingt-neuf", num.toLetters());
        
    }
    
}
