import java.util.ArrayList;

public class Numeral {
    private int value;

    public Numeral (String number){
        this.value = Integer.parseInt(number);
    }
    public String toLetters () {
        String ret = "";
        if (this.value <= 16 && this.value >= 0){
            return getUnit(this.value);
        }

        if (this.value/10 >= 1 && this.value/10 <= 8 && this.value/10 != 7){
            ret += decade(this.value/10);
        } else if (this.value/10 == 7 || this.value/10 == 9) {
            ret += decade((this.value/10)-1);
        }

        if (this.value%10 != 0){
            if ((this.value/10 != 0 && this.value%10 != 1) || this.value/10 == 9){
                ret += "-";
            } else if (this.value/10 != 0 && this.value%10 == 1){
                ret += "-et-";
            }

            if ((this.value/10 == 7 && this.value%10 < 7 && this.value%10 > 0) || (this.value/10 == 9 && this.value%10 < 7 && this.value%10 > 0)){
                ret += getUnit((this.value%10)+10);

            } else if (this.value/10 == 7 || this.value/10 == 9) {
                ret += decade(1);
                ret += "-";
                ret += getUnit(value%10);
            }    
            else {
                ret += getUnit(this.value%10);
            }
        }

        if (this.value/10 == 8 && this.value%10 == 0){
            ret += "s";
        }
      
        return ret;
    }

    private String getUnit(int value) {
        NumberEnum ne = new NumberEnum();
        ArrayList<String> list = ne.getListLetters();
        return list.get(value);
    }

    private String decade(int value){
        NumberEnum ne = new NumberEnum();
        ArrayList<String> list = ne.getListDecade();
        return list.get(value);
    }

    private String hundred(int value){
        NumberEnum ne = new NumberEnum();
        ArrayList<String> list = ne.getListHundred();
        return list.get(value);
    }

    public int getValue() {
        return value;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i ++){
            Numeral n = new Numeral(String.valueOf(i));
            System.out.println(n.toLetters() + "\n");
        }
    }
}