import java.util.ArrayList;

public class NumberEnum {
    ArrayList<String> listLetters;
    ArrayList<String> listDecade;
    ArrayList<String> listHundred;

    public NumberEnum(){
        this.listLetters = new ArrayList<String>();
        createListLetter();
        this.listDecade = new ArrayList<String>();
        createListDecade();
        this.listHundred = new ArrayList<String>();
        createListHundred();
    }
    
    public ArrayList<String> getListLetters() {
        return listLetters;
    }

    public ArrayList<String> getListDecade() {
        return listDecade;
    }

    public ArrayList<String> getListHundred() {
        return listHundred;
    }

    private void createListLetter(){
        listLetters.add("zéro");
        listLetters.add("un");
        listLetters.add("deux");
        listLetters.add("trois");
        listLetters.add("quatre");
        listLetters.add("cinq");
        listLetters.add("six");
        listLetters.add("sept");
        listLetters.add("huit");
        listLetters.add("neuf");
        listLetters.add("dix");
        listLetters.add("onze");
        listLetters.add("douze");
        listLetters.add("treize");
        listLetters.add("quatorze");
        listLetters.add("quinze");
        listLetters.add("seize");
    }

    private void createListDecade(){
        listDecade.add("zéro");
        listDecade.add("dix");
        listDecade.add("vingt");
        listDecade.add("trente");
        listDecade.add("quarante");
        listDecade.add("cinquante");
        listDecade.add("soixante");
        listDecade.add("soixante-dix");
        listDecade.add("quatre-vingt");
        listDecade.add("quatre-vingt-dix");
    }

    private void createListHundred(){
        listDecade.add("zéro");
        listDecade.add("cent");
        listDecade.add("deux cent");
        listDecade.add("trois cent");
        listDecade.add("quatre cent");
        listDecade.add("cinq cent");
        listDecade.add("six cent");
        listDecade.add("sept cent");
        listDecade.add("huit cent");
        listDecade.add("neuf cent");
    }

    
}
